#ifndef RDI_DEFER_HPP
#define RDI_DEFER_HPP

/// stolen from mn
/// Mostafa Saad's minimal support library
/// https://github.com/MoustaphaSaad/mn

namespace RDI
{
	template <typename F>
	struct Defer
	{
		F f;
		Defer(F f) : f(f) {}
		~Defer() { f(); }
	};

	template<typename F>
	inline static Defer<F>
	make_defer(F f)
	{
		return Defer<F>(f);
	}

	#define rdi_DEFER_1(x, y) x##y
	#define rdi_DEFER_2(x, y) rdi_DEFER_1(x, y)
	#define rdi_DEFER_3(x)    rdi_DEFER_2(x, __COUNTER__)
	#define rdi_defer(code)   auto rdi_DEFER_3(_defer_) = RDI::make_defer([&](){code;})
}

#endif // RDI_DEFER_HPP
