#include "rdi_stl_utils.hpp"
#include "rdi_panic.hpp"

namespace RDI {

BlackHole::BlackHole()
{
	std::lock_guard<std::mutex> lock(m);
	#ifdef __linux__
	std::string stream_path = "/dev/null";
	#else
	std::string stream_path = "nul"; // nul is equivalent to /dev/null on windows
	#endif

	fflush(stderr);
	fgetpos(stderr, &stderr_.pos);
	stderr_.file_descriptor = dup(fileno(stderr));
	freopen(stream_path.data(), "w", stderr);
}

void BlackHole::back_to_normal_state()
{
	std::lock_guard<std::mutex> lock(m);
	// This means stderr in it is normal state
	if(stderr_.file_descriptor == -1) return;

	fflush(stderr);
	dup2(stderr_.file_descriptor, fileno(stderr));
	close(stderr_.file_descriptor);
	clearerr(stderr);
	fsetpos(stderr, &stderr_.pos);
	stderr_.file_descriptor = -1;
}

BlackHole::~BlackHole()
{
	back_to_normal_state();
}

}
