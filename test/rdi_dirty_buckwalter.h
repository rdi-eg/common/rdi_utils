#ifndef RDI_DIRTY_BUCKWALTER_H
#define RDI_DIRTY_BUCKWALTER_H

#define S_FASLA std::wstring(L"\u060C")
#define S_QUESTION_MARK std::wstring(L"\u061F")
#define S_FASLA_MANKOOTA std::wstring(L"\u061B")
#define S_HAMZA std::wstring(L"\u0621")
#define S_ALEF_MADDA std::wstring(L"\u0622")
#define S_ALEF_HAMZA_ABOVE std::wstring(L"\u0623")
#define S_WAW_HAMZA_ABOVE std::wstring(L"\u0624")
#define S_ALEF_HAMZA_BELOW std::wstring(L"\u0625")
#define S_YEH_HAMZA_ABOVE std::wstring(L"\u0626")
#define S_ALEF_NO_HAMZA std::wstring(L"\u0627")
#define S_BEH std::wstring(L"\u0628")
#define S_TEH_MARBOOTA std::wstring(L"\u0629")
#define S_TEH std::wstring(L"\u062A")
#define S_THEH std::wstring(L"\u062B")
#define S_JEEM std::wstring(L"\u062C")
#define S_HAH std::wstring(L"\u062D")
#define S_KHAH std::wstring(L"\u062E")
#define S_DAL std::wstring(L"\u062F")
#define S_THAL std::wstring(L"\u0630")
#define S_REH std::wstring(L"\u0631")
#define S_ZAIN std::wstring(L"\u0632")
#define S_SEEN std::wstring(L"\u0633")
#define S_SHEEN std::wstring(L"\u0634")
#define S_SAD std::wstring(L"\u0635")
#define S_DAD std::wstring(L"\u0636")
#define S_TAH std::wstring(L"\u0637")
#define S_ZAH std::wstring(L"\u0638")
#define S_AIN std::wstring(L"\u0639")
#define S_GHAIN std::wstring(L"\u063A")
#define S_FEH std::wstring(L"\u0641")
#define S_QAF std::wstring(L"\u0642")
#define S_KAF std::wstring(L"\u0643")
#define S_LAM std::wstring(L"\u0644")
#define S_MEEM std::wstring(L"\u0645")
#define S_NOON std::wstring(L"\u0646")
#define S_HEH std::wstring(L"\u0647")
#define S_WAW std::wstring(L"\u0648")
#define S_YEH_NO_DOTS std::wstring(L"\u0649")
#define S_YEH_WITH_DOTS std::wstring(L"\u064A")
#define S_TATWEEL std::wstring(L"\u0640")
#define S_TANWEEN_FATHA std::wstring(L"\u064b")
#define S_TANWEEN_DAMA std::wstring(L"\u064c")
#define S_TANWEEN_KASRA std::wstring(L"\u064d")
#define S_FATHA std::wstring(L"\u064e")
#define S_DAMA std::wstring(L"\u064f")
#define S_KASRA std::wstring(L"\u0650")
#define S_SHADA std::wstring(L"\u0651")
#define S_SKOON std::wstring(L"\u0652")

#endif // RDI_DIRTY_BUCKWALTER_H
